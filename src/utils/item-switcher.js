import { mockdata } from '../api/mockdata';
import { setupItem } from '../components/main/item/item';

let currentItemIndex = -1;
let currentItem = null;
export const switchItem = (parent, switchDirection) => {
    // Destroy current item if it exists
    if (currentItem) {
        currentItem.remove();
    }

    if (switchDirection === 'right' && currentItemIndex < mockdata.items.length - 1) {
        currentItemIndex++;
    } else if (switchDirection === 'left' && currentItemIndex > 0) {
        currentItemIndex--;
    }
    else if (switchDirection === 'right') {
        currentItemIndex = 0;
    } else if (switchDirection === 'left') {
        currentItemIndex = mockdata.items.length - 1;
    }

    // Save the current item
    currentItem = setupItem(parent, mockdata.items[currentItemIndex]);
};
