import { createElement } from '../../utils';

export function setupMain(parent) {
    return createElement('main', parent, {
        class: 'main',
    });
}
