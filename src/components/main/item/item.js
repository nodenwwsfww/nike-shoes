import { createElement } from '../../../utils';
import { images } from '../../../api/images';
import './item.css';

export function setupItem(parent, itemData) {
    const item = createElement('section', parent, {
        class: 'item',
    });

    const itemMainView = createElement('div', item, {
        class: 'item__main-view',
    });

    createElement('img', itemMainView, {
        class: 'item__img',
        src: itemData.src,
        alt: itemData.title,
        width: itemData.width,
        height: itemData.height});



    const itemTitle = createElement('span', itemMainView, {
        class: 'item__title'}, itemData.title);

    itemTitle.style.WebkitTextStrokeWidth = `${itemData.titleTextStrokeWidth}`;
    itemTitle.style.WebkitTextStrokeColor = `${itemData.titleColor}`;

    // Create a <style> element because pseudo elements are not accessible from JS
    const itemTitleStyle = document.createElement('style');

    // Add a CSS rule with the dynamic content
    itemTitleStyle.innerHTML = `
      .item__title::before {
        position: absolute;
        content: "${itemData.title}";
        color: ${itemData.titleColor};
        z-index: -1;
      }
    `;
    item.appendChild(itemTitleStyle);


    const itemFooter = createElement('div', item, {
        class: 'item__footer',
    });

    const itemColumnContentContainer = createElement('div', itemFooter, { class: 'item-column-content-container'});

    createElement('span', itemColumnContentContainer, { class: 'item__price'}, `$${itemData.price}`);
    const ratingStarsRow = createElement('div', itemColumnContentContainer, { class: 'item__rating'});

    for (let i = 0; i < itemData.rating; i++) {
        createElement('img', ratingStarsRow, { class: 'item__star', src: images.item.filledStar, alt: 'star'});
    }

    // Fills only in case when rating is less than maxRating
    for (let i = 0; i < itemData.maxRating - itemData.rating; i++) {
        createElement('img', ratingStarsRow, { class: 'item__star', src: images.item.notFilledStar, alt: 'star'});
    }

    const itemRowContentContainer = createElement('div', itemFooter, { class: 'item-row-content-container'});

    const itemDescription = createElement('p', itemRowContentContainer, { class: 'item__description'}, itemData.description);
    let textFromServer = itemData.description;

    let lines = textFromServer.split('\n'); // split the text into an array of lines

    // clear any existing content
        while (itemDescription.firstChild) {
            itemDescription.removeChild(itemDescription.firstChild);
        }

    // for each line, create a text node and a <br> element
        lines.forEach((line, i) => {
            let textNode = document.createTextNode(line);
            itemDescription.appendChild(textNode);

            // Don't append a <br> after the last line
            if (i < lines.length - 1) {
                itemDescription.appendChild(document.createElement('br'));
            }
        });

    const btnContainer = createElement('div', itemRowContentContainer, { class: 'item-btn-container'});
    createElement('button', btnContainer, { class: 'item-btn-container__btn'}, 'BUY NOW');
    return item;
}
